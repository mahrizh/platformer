﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace UI
{
	[AddComponentMenu("UI/Effects/Motion Glitch")]
	public class UIEffectMotionGlitch : BaseMeshEffect
	{
		[SerializeField]
		[Range(0, 32)]
		private int count = 1;

		[SerializeField]
		private Vector2 velocity;

		[SerializeField]
		private Gradient gradient;
		
		private RectTransform rectTransform;

		private readonly Queue<int> snapshotSizes = new Queue<int>();
		private readonly List<UIVertex> vertices = new List<UIVertex>();

		private Vector2 oldPosition;

		bool x;

		public override void ModifyMesh(VertexHelper vh)
		{
			List<UIVertex> verts = new List<UIVertex>();
			vh.GetUIVertexStream(verts);

			if (IsActive())
			{
				if (rectTransform == null)
				{
					rectTransform = GetComponent<RectTransform>();
					oldPosition = rectTransform.anchoredPosition + velocity * Time.unscaledDeltaTime;
				}

				var shift = (Vector3)(oldPosition - rectTransform.anchoredPosition);
				for (int i = 0; i < vertices.Count; ++i)
				{
					var vertex = vertices[i];
					vertex.position += shift;
					vertex.color = gradient.Evaluate(1 - (float)i/(float)vertices.Count);
					vertices[i] = vertex;
				}
				oldPosition = rectTransform.anchoredPosition + velocity * Time.unscaledDeltaTime;

				while (snapshotSizes.Count > count)
					vertices.RemoveRange(0, snapshotSizes.Dequeue());

				snapshotSizes.Enqueue(verts.Count);
				vertices.AddRange(verts);

				foreach (var snapshot in vertices)
					vh.AddUIVertexTriangleStream(vertices);
			}
		}

		public override bool IsActive() => base.IsActive() && count > 0;

		private void Update() => OnDidApplyAnimationProperties();
	}
}