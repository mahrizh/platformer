﻿using Gameplay;
using UnityEngine;
using Common.Singletons;

namespace UI
{
	public class UIController : SingletonBehaviour<UIController>
	{
		[SerializeField]
		private GameObject hudScreen;

		[SerializeField]
		private GameObject pauseScreen;

		[SerializeField]
		private GameObject gameOverScreen;

		private void Start()
		{
			GameplayController.Instance.Player.OnDie.AddListener(ShowGameOver);
			Reset();
		}

		private void Reset()
		{
			hudScreen?.SetActive(true);
			gameOverScreen?.SetActive(false);
			pauseScreen?.SetActive(false);
		}

		public void ShowPause(bool show)
		{
			hudScreen?.SetActive(!show || !gameOverScreen.activeSelf);
			pauseScreen?.SetActive(show);
		}

		public void ShowGameOver()
		{
			hudScreen?.SetActive(false);
			gameOverScreen?.SetActive(true);
		}

		public void RestartGame()
		{
			Reset();
			MainController.Instance.Restart();
		}
	}
}
