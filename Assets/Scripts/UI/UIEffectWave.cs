﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Common.Constants;

namespace UHL
{
	[AddComponentMenu("UI/Effects/Wave")]
	public class UIEffectWave : BaseMeshEffect
	{
		[SerializeField]
		private Vector2 amplitude = Vector2.one;

		[SerializeField]
		private Vector2 period = Vector2.one * 0.1f;

		[SerializeField]
		private Vector2 speed = Vector2.one;

		[SerializeField]
		private bool transverseWaveX = true;

		[SerializeField]
		private bool transverseWaveY = true;

		public override void ModifyMesh(VertexHelper vh)
		{
			List<UIVertex> verts = new List<UIVertex>();
			vh.GetUIVertexStream(verts);

			if (IsActive() && (Mathf.Abs(amplitude.x) > Const.Tiny || Mathf.Abs(amplitude.y) > Const.Tiny))
			{
				for (int i = 0; i < verts.Count; ++i)
				{
					var vert = verts[i];

					var coord = new Vector2(
						transverseWaveX ? vert.position.y : vert.position.x,
						transverseWaveY ? vert.position.x : vert.position.y
					);

					vert.position.x += Mathf.Sin(Time.unscaledTime * speed.x + coord.x * period.x) * amplitude.x;
					vert.position.y += Mathf.Sin(Time.unscaledTime * speed.y + coord.y * period.y) * amplitude.y;
					verts[i] = vert;
				}
				
				vh.AddUIVertexTriangleStream(verts);
			}
		}

		private void Update()
		{
			OnDidApplyAnimationProperties();
		}
	}
}