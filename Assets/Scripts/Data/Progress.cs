﻿using System.Collections.Generic;
using UnityEngine;
using Common.Singletons;

namespace Data
{
	public class Progress : SingletonScriptableObject<Progress>
	{
		[System.Serializable]
		public struct CheckPoint
		{
			[SerializeField]
			private int levelNumber;
			public int LevelIndex => levelNumber;

			[SerializeField]
			private int checkPointId;
			public int CheckPointId => checkPointId;

			public CheckPoint(int levelNumber, int checkPointId)
			{
				this.levelNumber = levelNumber;
				this.checkPointId = checkPointId;
			}
		}

		[System.Serializable]
		private class Savable
		{
			public List<CheckPoint> checkPoints;
			public CheckPoint currentCheckpoint;

			public Savable()
			{
				checkPoints = new List<CheckPoint>();
				currentCheckpoint = new CheckPoint(1, 0);
			}
		}

		private Savable data;

		public CheckPoint CurrentCheckPoint
		{
			get => data.currentCheckpoint;
			set
			{
				data.currentCheckpoint = value;
				Save();
			}
		}

		public void AddCheckPoint(CheckPoint checkPoint)
		{
			var same = data.checkPoints.Find(cp => cp.Equals(checkPoint));
			if (same.Equals(checkPoint))
				return;

			data.checkPoints.Add(checkPoint);
			Save();
		}

		private void OnEnable()
		{
			Load();
		}

		private const string SaveKey = "__progress__";

		private void Load()
		{
			if (PlayerPrefs.HasKey(SaveKey))
			{
				var json = PlayerPrefs.GetString(SaveKey);
				data = JsonUtility.FromJson<Savable>(json);
			}

			if (data == null)
				data = new Savable();
		}

		private void Save()
		{
			var json = JsonUtility.ToJson(data, false);
			PlayerPrefs.SetString(SaveKey, json);
		}

#if UNITY_EDITOR
		[UnityEditor.MenuItem(DefaultAssetMenuPath + nameof(Progress))]
		private static void Create() => CreateAsset(out _);
#endif
	}
}
