﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

namespace Common.Singletons
{
	internal static class SingletonAssetUtility
	{
		private enum AssetType
		{
			Unknown,
			Prefab,
			Asset
		}

		public const string DefaultMenuPath = "Assets/Create/Singleton/";

		/// Returns true if new asset was created.
		public static bool Create<T>(string path, out T asset)
			where T : Object
		{
			AssetType assetType =
				typeof(T).IsSubclassOf(typeof(ScriptableObject)) ? AssetType.Asset :
				typeof(T).IsSubclassOf(typeof(MonoBehaviour)) ? AssetType.Prefab :
				/* else */ AssetType.Unknown;

			path = string.Format("Assets/Resources/{0}.{1}", path, assetType.ToString().ToLower());
			asset = AssetDatabase.LoadAssetAtPath<T>(path);

			if (asset && !EditorUtility.DisplayDialog("An asset already exists.", "Overwrite it?", "Yes", "No"))
			{
				return false;
			}

			string[] pathParts = path.Split('/');
			string createdFoldersPath = pathParts[0];
			for (int i = 1; i < pathParts.Length - 1; ++i)
			{
				var nextFolderPath = string.Format("{0}/{1}", createdFoldersPath, pathParts[i]);
				if (!AssetDatabase.IsValidFolder(nextFolderPath))
				{
					AssetDatabase.CreateFolder(createdFoldersPath, pathParts[i]);
				}
				createdFoldersPath = nextFolderPath;
			}

			switch (assetType)
			{
				case AssetType.Asset:
					asset = ScriptableObject.CreateInstance(typeof(T)) as T;
					AssetDatabase.CreateAsset(asset, path);
					AssetDatabase.SaveAssets();
					break;
				case AssetType.Prefab:
					{
						GameObject sceneGameObject = new GameObject(typeof(T).Name, typeof(T));
						GameObject assetGameObject = PrefabUtility.SaveAsPrefabAsset(sceneGameObject, path);
						asset = assetGameObject.GetComponent<T>();
						Object.DestroyImmediate(sceneGameObject);
						break;
					}
				default:
					Debug.LogError("Unknown asset type: " + typeof(T).Name);
					return false;
			}

			EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;

			return true;
		}
	}
}
#endif
