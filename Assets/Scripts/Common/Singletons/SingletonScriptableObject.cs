﻿using UnityEngine;

namespace Common.Singletons
{
	/// <summary> ScriptableObject singleton implementation. </summary>
	/// <typeparam name="T">A class inherited from SingletonScriptableObject.</typeparam>
	/// <example> Usage:
	/// <code>
	/// public class ExampleInfo : SingletonBehaviour&lt;ExampleInfo&gt;
	/// {
	///		[SerializeField]
	///		private Vector3[] data;
	/// 
	/// #if UNITY_EDITOR
	///		[UnityEditor.MenuItem(DefaultAssetMenuPath + nameof(ExampleInfo))]
	///		private static void Create() =&gt; CreateAsset(out _);
	/// #endif
	/// }
	/// </code>
	/// </example>
	[DefaultExecutionOrder(int.MinValue / 2)]
	public class SingletonScriptableObject<T> : ScriptableObject
		where T : SingletonScriptableObject<T>
	{
		/// <summary>Unique asset path according namespace: "RootNS/SubNS/.../SubNS/ClassName".</summary>
		public static string AssetPath => typeof(T).ToString().Replace('.', '/');

		private static bool applicationQuit = false;
		private static T _instance;
		/// <summary>An instance of a singleton.</summary>
		public static T Instance
		{
			get
			{
				if (_instance == null && !applicationQuit)
				{
					_instance = Resources.Load<T>(AssetPath);

					if (_instance == null)
					{
						Debug.LogError(typeof(T).Name + " cannot be instanced.");
					}
				}

				return _instance;
			}
		}

		/// <summary> Check instance exist. </summary>
		public static bool IsInstanced { get { return _instance; } }

		protected virtual void OnApplicationQuit()
		{
			applicationQuit = true;
		}

#if UNITY_EDITOR
		protected const string DefaultAssetMenuPath = SingletonAssetUtility.DefaultMenuPath;

		/// <summary>Creates asset according to unique path rule.</summary>
		/// <returns>True if new asset was created.</returns>
		/// <see cref="AssetPath"/>
		protected static bool CreateAsset(out T asset)
		{
			return SingletonAssetUtility.Create(AssetPath, out asset);
		}
#endif
	}
}
