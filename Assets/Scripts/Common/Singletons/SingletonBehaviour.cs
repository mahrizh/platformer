﻿using UnityEngine;
using UnityEngine.Events;
using Common.Events;

namespace Common.Singletons
{
	/// <summary> MonoBehaviour singleton implementation. </summary>
	/// <typeparam name="T">A class inherited from SingletonBehaviour.</typeparam>
	/// <example> Usage:
	/// <code>
	/// public class ExampleController : SingletonBehaviour&lt;ExampleController&gt;
	/// {
	///		[SerializeField]
	///		private Vector3[] data;
	/// 
	/// #if UNITY_EDITOR
	///		[UnityEditor.MenuItem(DefaultAssetMenuPath + nameof(ExampleController))]
	///		private static void Create() =&gt; CreateAsset(out _);
	/// #endif
	/// }
	/// </code>
	/// </example>
	/// 
	[DefaultExecutionOrder(int.MinValue / 2)]
	public class SingletonBehaviour<T> : MonoBehaviour
		where T : SingletonBehaviour<T>
	{
		/// <summary>Invokes when singleton instance is changed.</summary>
		public static readonly UnityEvent<T> OnInstanceChanged = new UnityEventConcrete<T>();

		/// <summary>Unique asset path according namespace: "RootNS/SubNS/.../SubNS/ClassName".</summary>
		public static string AssetPath => typeof(T).ToString().Replace('.', '/');

		private static bool applicationQuit = false;
		private static T _instance;

		/// <summary>An instance of a singleton.</summary>
		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					Instance = FindObjectOfType<T>();
					if (!applicationQuit)
					{
						if (_instance == null)
						{
							var prefab = Resources.Load<T>(AssetPath);
							if (prefab)
							{
								Instance = Instantiate(prefab);
								Debug.Log(typeof(T).Name + " instanced from resource.");
							}
						}

						if (_instance == null)
						{
							Instance = new GameObject().AddComponent<T>();
							Debug.Log(typeof(T).Name + " instanced from scratch.");
						}

						if (_instance)
						{
							_instance.name = typeof(T).Name;
						}
						else
						{
							Debug.LogError(typeof(T).Name + " cannot be instanced.");
						}
					}
				}
				return _instance;
			}
			private set { OnInstanceChanged.Invoke(_instance = value); }
		}

		/// <summary> Check instance exist. </summary>
		public static bool IsInstanced { get { return _instance; } }

		/// <summary>Override it to controll auto destroying object on scene unload.</summary>
		protected virtual bool Destroyable { get { return false; } }

		protected virtual void Awake()
		{
			if (_instance == null)
			{
				Instance = this as T;
				if (!Destroyable)
				{
					transform.SetParent(null);
					DontDestroyOnLoad(gameObject);
				}
			}
			else if (Instance != this)
			{
				Debug.LogWarning(typeof(T).Name + " instance duplication.");
				Destroy(gameObject);
			}
		}

		protected virtual void OnDestroy()
		{
			if (Instance == this)
			{
				Instance = null;
			}
		}

		protected virtual void OnApplicationQuit()
		{
			applicationQuit = true;
		}

#if UNITY_EDITOR
		protected const string DefaultAssetMenuPath = SingletonAssetUtility.DefaultMenuPath;

		/// <summary>Creates asset according to unique path rule.</summary>
		/// <returns>True if new asset was created.</returns>
		/// <see cref="AssetPath"/>
		protected static bool CreateAsset(out T asset)
		{
			return SingletonAssetUtility.Create(AssetPath, out asset);
		}
#endif
	}
}
