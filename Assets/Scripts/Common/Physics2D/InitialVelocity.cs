﻿using UnityEngine;

namespace Common.Physics2D
{
	[AddComponentMenu("Physics 2D/Initial Velocity")]
	[RequireComponent(typeof(Rigidbody2D))]
	public class InitialVelocity : MonoBehaviour
	{
		[SerializeField]
		private Vector2 worldVelocity;

		[SerializeField]
		private Vector2 localVelocity;

		[SerializeField]
		private float angularVelocity;

		void OnEnable()
		{
			var body = GetComponent<Rigidbody2D>();
			body.velocity = worldVelocity + (Vector2)transform.TransformVector(localVelocity);
			body.angularVelocity = angularVelocity;
		}
	}
}
