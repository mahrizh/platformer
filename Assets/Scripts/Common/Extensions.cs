﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Extensions
{
	public static class Extensions
	{
		internal static T GetRandom<T>(this IReadOnlyList<T> self)
			=> self.Count > 0
			? self[Random.Range(0, self.Count)]
			: default;
	}
}
