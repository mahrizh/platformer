﻿using UnityEngine;

namespace Common.Constants
{
	public static class Const
	{
		/// <summary>Enough tiny float, but not too.</summary>
		public const float Tiny = 1f / 4096f;

		/// <summary> Half of Pi. </summary>
		public const float PiHalf = Mathf.PI * 0.5f;

		/// <summary> Double Pi. </summary>
		public const float PiDouble = Mathf.PI * 2;

		/// <summary> e </summary>
		public const float E = 2.718281828f;

		/// <summary>Golden = C/B = B/A, where A + B = C.</summary>
		public const float Golden = 1.618033989f;

		/// <summary>Equals Golden^-1</summary>
		/// <see cref="Golden"/>
		public const float GoldenInv = Golden - 1;

		/// <summary>Equals Golden^-2</summary>
		/// <see cref="Golden"/>
		public const float GoldenInvSq = 1 - GoldenInv;

		/// <summary>Rect from (0, 0) to (1, 1).</summary>
		public static readonly Rect UnitRect = Rect.MinMaxRect(0, 0, 1, 1);
	}

	public static class WaitFor
	{
		/// <summary>Constant to reduce memory allocation.</summary>
		public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();

		/// <summary>Constant to reduce memory allocation.</summary>
		public static readonly WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();

		/// <summary>Constant to reduce memory allocation.</summary>
		public static readonly WaitForSeconds Second = new WaitForSeconds(1);
	}
}
