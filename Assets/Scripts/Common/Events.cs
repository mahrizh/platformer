﻿using UnityEngine.Events;
using UnityEngine;

namespace Common.Events
{
	[System.Serializable]
	public class UnityEventBool : UnityEvent<bool> { }

	[System.Serializable]
	public class UnityEventInt : UnityEvent<int> { }

	[System.Serializable]
	public class UnityEventFloat : UnityEvent<float> { }

	[System.Serializable]
	public class UnityEventVector2 : UnityEvent<Vector2> { }

	[System.Serializable]
	public class UnityEventVector2Int : UnityEvent<Vector2Int> { }

	[System.Serializable]
	public class UnityEventVector3 : UnityEvent<Vector3> { }

	[System.Serializable]
	public class UnityEventVector3Int : UnityEvent<Vector3Int> { }

	[System.Serializable]
	public class UnityEventQuaternion : UnityEvent<Quaternion> { }

	[System.Serializable]
	public class UnityEventColor : UnityEvent<Color> { }

	[System.Serializable]
	public class UnityEventString : UnityEvent<string> { }

	[System.Serializable]
	public class UnityEventConcrete<T> : UnityEvent<T> { }

	[System.Serializable]
	public class UnityEventConcrete<T0, T1> : UnityEvent<T0, T1> { }

	[System.Serializable]
	public class UnityEventConcrete<T0, T1, T2> : UnityEvent<T0, T1, T2> { }

	[System.Serializable]
	public class UnityEventConcrete<T0, T1, T2, T3> : UnityEvent<T0, T1, T2, T3> { }
}
