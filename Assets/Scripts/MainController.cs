﻿using Data;
using Gameplay;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Common.Events;
using Common.Singletons;

public class MainController : SingletonBehaviour<MainController>
{
	private const float sceneProgressMax = 0.9f;

	public readonly UnityEvent<float> OnLoadingProgress = new UnityEventFloat();

	public int CurrentLevelIndex => SceneManager.GetActiveScene().buildIndex;

	public void GoTo(Progress.CheckPoint point)
	{
		if (goToRoutine == null)
			goToRoutine = StartCoroutine(GoToRoutine(point));
	}

	Coroutine goToRoutine = null;
	public IEnumerator GoToRoutine(Progress.CheckPoint point)
	{
		float progress = 0;
		OnLoadingProgress.Invoke(progress);

		Progress.Instance.CurrentCheckPoint = point;

		if (CurrentLevelIndex != point.LevelIndex)
		{
			var loading = SceneManager.LoadSceneAsync(point.LevelIndex);
			while (!loading.isDone)
			{
				progress = loading.progress * sceneProgressMax;
				OnLoadingProgress.Invoke(progress);
				yield return null;
			}
		}

		while (!GameplayController.IsInstanced)
			yield return null;

		var gameplay = GameplayController.Instance;

		gameplay.Reset(point.CheckPointId);

		while (!gameplay.IsInitialized)
		{
			progress += (1 - progress) * Time.deltaTime * 0.1f;
			OnLoadingProgress.Invoke(progress);
			yield return null;
		}

		OnLoadingProgress.Invoke(1);
		goToRoutine = null;
	}
	
	public void Restart()
		=> GoTo(Progress.Instance.CurrentCheckPoint);

	private void Start()
	{
		if (CurrentLevelIndex <= 0)
			Restart();
	}

#if UNITY_EDITOR
	[UnityEditor.MenuItem(DefaultAssetMenuPath + nameof(MainController))]
	private static void Create() => CreateAsset(out _);
#endif
}
