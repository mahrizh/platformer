﻿using UnityEngine;
using UnityEngine.Events;
using Common.Extensions;

namespace Gameplay
{
	public class AnimationEventWrapper : MonoBehaviour
	{
		[SerializeField]
		private UnityEvent[] animationEvents;

		public void InvokeInt(int index)
		{
			if (0 <= index && index < animationEvents.Length)
				animationEvents[index].Invoke();
		}

		public void InvokeAnimationEvent(AnimationEvent evnt)
		{
			if (0 <= evnt.intParameter && evnt.intParameter < animationEvents.Length)
				animationEvents[evnt.intParameter].Invoke();
		}

		public void InvokeAny()
		{
			animationEvents.GetRandom()?.Invoke();
		}
	}
}
