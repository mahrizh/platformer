﻿using UnityEngine;

namespace Gameplay
{
	[RequireComponent(typeof(Collider2D))]
	public class DamageTrigger : MonoBehaviour
	{
		private Collider2D shape;

		private int collidersBufferLength = 0;
		private readonly Collider2D[] collidersBuffer = new Collider2D[10];
		private readonly ContactFilter2D contactFilter = new ContactFilter2D();

		public void ForceInteract()
		{
			if (shape)
			{
				collidersBufferLength = shape.OverlapCollider(contactFilter, collidersBuffer);

				for (int i = 0; i < collidersBufferLength; ++i)
					Interact(collidersBuffer[i].GetComponent<Unit>());
			}
		}

		private void Awake()
		{
			shape = GetComponent<Collider2D>();
		}

		private void Start() { /* To show Enabled checkbox in inspector*/ }

		private void OnTriggerEnter2D(Collider2D coliider)
		{
			if (enabled)
				Interact(coliider.GetComponent<Unit>());
		}

		private void Interact(Unit unit)
		{
			if (unit)
				--unit.Health;
		}
	}
}
