﻿using UnityEngine;

namespace Gameplay
{
	[RequireComponent(typeof(PlayerCamera))]
	public class OutOfViewKiller : MonoBehaviour
	{
		private new PlayerCamera camera;

		private void Awake()
		{
			camera = GetComponent<PlayerCamera>();
		}

		private void FixedUpdate()
		{
			if (camera.player)
			{
				if (camera.IsFixedX)
				{
					float distance = Mathf.Abs(camera.player.transform.position.x - transform.position.x);
					if (distance > camera.Camera.orthographicSize * camera.Camera.aspect)
					{
						camera.player.Health = 0;
						return;
					}
				}

				if (camera.IsFixedY)
				{
					float distance = Mathf.Abs(camera.player.transform.position.y - transform.position.y);
					if (distance > camera.Camera.orthographicSize)
					{
						camera.player.Health = 0;
						return;
					}
				}
			}
		}
	}
}
