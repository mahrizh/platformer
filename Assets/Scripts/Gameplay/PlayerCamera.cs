﻿using UnityEngine;

namespace Gameplay
{
	[RequireComponent(typeof(Camera))]
	public class PlayerCamera : MonoBehaviour
	{
		public PlatformUnit player;

		[SerializeField]
		[Range(0, 1)]
		private float lookAhead = 0.25f;

		[SerializeField]
		[Range(1, 100)]
		private float rowHeight = 10;
		
		[SerializeField]
		private Vector2 fixedAxis;
		public bool IsFixedX
		{
			get => !float.IsInfinity(fixedAxis.x);
			set => fixedAxis.x = value ? transform.position.x : float.PositiveInfinity;
		}
		public bool IsFixedY
		{
			get => !float.IsInfinity(fixedAxis.y);
			set => fixedAxis.y = value ? transform.position.y : float.PositiveInfinity;
		}
		public void Fix(float x = float.PositiveInfinity, float y = float.PositiveInfinity)
		{
			fixedAxis.x = x;
			fixedAxis.y = y;
		}

		public Camera Camera { get; private set; }

		private Vector3 cameraVelocity;
		private float cameraCenterShift;

		private void Awake()
		{
			Camera = GetComponent<Camera>();
		}

		private void LateUpdate()
		{
			if (player == null)
				return;

			float moveRatio = Mathf.Abs(player.Body.velocity.x) / player.MoveSpeed;

			float targetCameraCenterShift = (player.LookRight ? lookAhead : -lookAhead) * Camera.orthographicSize * Camera.aspect;
			cameraCenterShift += (targetCameraCenterShift - cameraCenterShift) * Time.deltaTime * moveRatio;

			Vector3 targetPosition = player.transform.position;
			targetPosition.x += cameraCenterShift;
			targetPosition.z = -10;

			if (IsFixedX)
				targetPosition.x = fixedAxis.x;
			if (IsFixedY)
				targetPosition.y = fixedAxis.y;
			
			var position = Vector3.SmoothDamp(transform.position, targetPosition, ref cameraVelocity, 0.1f, float.MaxValue, Time.unscaledDeltaTime);
			position.y = targetPosition.y;

			transform.position = position;
		}
	}
}
