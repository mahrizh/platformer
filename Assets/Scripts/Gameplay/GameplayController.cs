﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Common.Singletons;

namespace Gameplay
{
	public class GameplayController : SingletonBehaviour<GameplayController>
	{
		protected override bool Destroyable => true;

		[SerializeField]
		private CheckPoint[] checkPoints;

		[SerializeField]
		private PlatformUnit player;
		public PlatformUnit Player => player;

		public readonly UnityEvent OnReset = new UnityEvent();

		public bool IsInitialized { get; private set; }

		private IEnumerator Start()
		{
			if (checkPoints == null || checkPoints.Length == 0)
			{
				checkPoints = FindObjectsOfType<CheckPoint>();
				System.Array.Sort(checkPoints, (l, r) => l.name.CompareTo(r.name));
			}

			yield return null;

			IsInitialized = true;

#if UNITY_EDITOR
			MainController.Instance.Restart();
#endif
		}

		public void Reset(int checkPointId)
		{
			var checkPoint = checkPoints[Mathf.Clamp(checkPointId, 0, checkPoints.Length - 1)];
			player.transform.position = checkPoint.RestorePlace.position;
			player.Reset();
			OnReset.Invoke();
		}

		public int GetCheckPointIndex(CheckPoint checkPoint) => System.Array.IndexOf(checkPoints, checkPoint);
	}
}