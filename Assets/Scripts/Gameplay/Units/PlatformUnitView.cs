﻿using UnityEngine;

namespace Gameplay
{
	public class PlatformUnitView : MonoBehaviour
	{
		[SerializeField]
		private new SpriteRenderer renderer;

		[SerializeField]
		private Animator animator;

		[SerializeField]
		private ParticleSystem moveParticles;

		[SerializeField]
		private ParticleSystem landBodyParticles;

		[SerializeField]
		private ParticleSystem landHeadParticles;

		[SerializeField]
		private ParticleSystem jumpParticles;

		[SerializeField]
		private ParticleSystem deathParticles;

		[SerializeField]
		[Range(0.1f, 2)]
		private float landParticlesRate = 1;

		PlatformUnit unit;

		private void Start()
		{
			unit = GetComponentInParent<PlatformUnit>();
			if (unit)
			{
				if (renderer)
					unit.OnLookRight.AddListener(right => renderer.flipX = !right);

				unit.OnGrounded.AddListener(OnGrounded);
				OnGrounded(unit.IsGrounded);

				unit.OnHeadGrounded.AddListener(OnHeadGrounded);
				OnHeadGrounded(unit.IsHeadGrounded);

				unit.OnJump.AddListener(OnJump);

				unit.OnAlive.AddListener(OnAlive);
				unit.OnDie.AddListener(OnDie);
			}
		}

		private void OnGrounded(bool grounded)
		{
			if (moveParticles)
			{
				var emission = moveParticles.emission;
				emission.enabled = grounded;
			}

			if (grounded && landBodyParticles)
				landBodyParticles.Emit(Mathf.RoundToInt(Mathf.Abs(unit.SmoothVelocity.y) * landParticlesRate));
		}

		private void OnHeadGrounded(bool grounded)
		{
			if (grounded && landHeadParticles)
				landHeadParticles.Emit(Mathf.RoundToInt(Mathf.Abs(unit.SmoothVelocity.y) * landParticlesRate));
		}

		private void OnJump(int jumpCount)
		{
			jumpParticles?.Emit(4);
		}

		private void OnAlive()
		{
			if (renderer)
				renderer.enabled = true;
		}

		private void OnDie()
		{
			deathParticles?.Emit(32);

			if (renderer)
				renderer.enabled = false;
		}
	}
}