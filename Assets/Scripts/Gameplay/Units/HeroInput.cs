﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
	[RequireComponent(typeof(PlatformUnit))]
	public class HeroInput : MonoBehaviour
	{
		private PlatformUnit hero;

		private void Awake()
		{
			hero = GetComponent<PlatformUnit>();
		}

		private void Update()
		{
			if (!hero.IsAlive)
				return;

			hero.MoveDirection = Input.GetAxis("Horizontal");

			if (Input.GetButtonDown("Jump"))
				hero.Jump();
		}
	}
}
