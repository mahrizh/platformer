﻿using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
	public class Unit : MonoBehaviour
	{
		[SerializeField]
		[Range(1, 1000)]
		private int healthMax = 1;
		public int HealthMax => healthMax;

		private int health;
		public int Health
		{
			get => health;
			set
			{
				bool willChangeVitality = (health > 0) != (value > 0);

				health = Mathf.Clamp(value, 0, healthMax);

				if (willChangeVitality)
					(IsAlive ? OnAlive : OnDie).Invoke();
			}
		}

		public bool IsAlive => health > 0;

		public readonly UnityEvent OnDie = new UnityEvent();
		public readonly UnityEvent OnAlive = new UnityEvent();

		protected virtual void Awake()
		{
			Reset();
		}

		public virtual void Reset()
		{
			Health = HealthMax;
		}
	}
}