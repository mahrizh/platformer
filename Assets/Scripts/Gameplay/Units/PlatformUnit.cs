﻿using UnityEngine;
using UnityEngine.Events;
using Common.Constants;
using Common.Events;

namespace Gameplay
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class PlatformUnit : Unit
	{
		[SerializeField]
		[Range(0, 100)]
		private float moveSpeed = 5;
		public float MoveSpeed => moveSpeed;

		[SerializeField]
		[Range(0, 1)]
		private float headAutoSpeedRatio = 0.95f;

		[SerializeField]
		[Range(0, 100)]
		private float jumpSpeed = 5;

		[SerializeField]
		[Range(0, 100)]
		private float fallSpeed = 5;

		[SerializeField]
		private BoxCollider2D bodyShape;

		[SerializeField]
		private BoxCollider2D headShape;

		private Rigidbody2D groundBody;

		[SerializeField]
		private bool lookRight = true;
		public bool LookRight
		{
			get => lookRight;
			set
			{
				var headOffset = headShape.offset;
				headOffset.x = value ? Mathf.Abs(headOffset.x) : -Mathf.Abs(headOffset.x);
				headShape.offset = headOffset;
				OnLookRight.Invoke(lookRight = value);
			}
		}
		
		public readonly UnityEvent<bool> OnGrounded = new UnityEventBool();
		public readonly UnityEvent<bool> OnHeadGrounded = new UnityEventBool();
		public readonly UnityEvent<bool> OnLookRight = new UnityEventBool();
		public readonly UnityEvent<int> OnJump = new UnityEventInt();

		private bool isGrounded;
		public bool IsGrounded
		{
			get => isGrounded;
			private set
			{
				if (isGrounded == value)
					return;

				if (!value)
					groundBody = null;

				OnGrounded.Invoke(isGrounded = value);
			}
		}
		private bool isHeadGrounded;
		public bool IsHeadGrounded
		{
			get => isHeadGrounded;
			private set
			{
				if (isHeadGrounded == value)
					return;

				if (!value)
					groundBody = null;

				OnHeadGrounded.Invoke(isHeadGrounded = value);
			}
		}

		public Rigidbody2D Body { get; private set; }

		public Vector2 SmoothVelocity { get; private set; }

		private float moveDirection;
		public float MoveDirection
		{
			get => moveDirection;
			set => moveDirection = Mathf.Clamp(value, -1, 1);
		}

		private bool isJumpFrame = false;
		private bool jumpRequested = false;
		public void Jump() => jumpRequested = true;
		public bool ExtraJump { get; set; }
		public int JumpCount { get; private set; }

		protected override void Awake()
		{
			base.Awake();
			Body = GetComponent<Rigidbody2D>();
			LookRight = LookRight;
			OnDie.AddListener(DoDie);
		}

		private void FixedUpdate()
		{
			isJumpFrame = false;
			var velocity = groundBody ? groundBody.velocity : new Vector2(0, Body.velocity.y);

			if (!IsHeadGrounded)
			{
				velocity.x += MoveDirection * moveSpeed;
				
				if (LookRight && MoveDirection < -Const.Tiny)
					LookRight = false;
				else if (!LookRight && MoveDirection > Const.Tiny)
					LookRight = true;
			}

			if (jumpRequested)
			{
				jumpRequested = false;
				if (IsGrounded || IsHeadGrounded)
				{
					IsGrounded = IsHeadGrounded = false;
					isJumpFrame = true;
					velocity.y = jumpSpeed;
					OnJump.Invoke(JumpCount = 1);
				}
				else if (ExtraJump)
				{
					ExtraJump = false;
					velocity.y = jumpSpeed;
					OnJump.Invoke(++JumpCount);
				}
			}

			velocity.y = Mathf.Max(velocity.y, -fallSpeed);

			Body.velocity = velocity;
			SmoothVelocity = (SmoothVelocity + velocity) * 0.5f;
		}

		private void OnCollisionEnter2D(Collision2D collision)
		{
			OnCollisionStay2D(collision);
		}

		private void OnCollisionStay2D(Collision2D collision)
		{
			if (isJumpFrame)
				return;

			bool upNormal = false;
			for (int i = 0; i < collision.contactCount; ++i)
			{
				var normal = collision.GetContact(i).normal;
				if (normal.y > Mathf.Abs(normal.x))
				{
					upNormal = true;
					break;
				}
			}

			if (upNormal)
			{
				if (collision.otherCollider == bodyShape)
				{
					IsGrounded = true;
					ExtraJump = true;
				}
				else if (collision.otherCollider == headShape)
				{
					IsHeadGrounded = true;
					ExtraJump = true;
				}
				groundBody = collision.rigidbody;
			}
		}

		private void OnCollisionExit2D(Collision2D collision)
		{
			if (collision.otherCollider == bodyShape)
				IsGrounded = false;
			else if (collision.otherCollider == headShape)
				IsHeadGrounded = false;
		}

		private void DoDie()
		{
			moveDirection = 0;
			jumpRequested = false;
		}
	}
}