﻿using UnityEngine;

namespace Gameplay.Interactives
{
	public class ResetableActivity : Resetable
	{
		[SerializeField]
		private bool isActive;

		protected override void Start()
		{
			base.Start();
			OnReset();
		}

		protected override void OnReset()
		{
			gameObject.SetActive(isActive);
		}
	}
}