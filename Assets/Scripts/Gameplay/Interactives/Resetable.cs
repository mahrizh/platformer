﻿using UnityEngine;

namespace Gameplay.Interactives
{
	public abstract class Resetable : MonoBehaviour
	{
		protected virtual void Start() => GameplayController.Instance.OnReset.AddListener(OnReset);

		protected abstract void OnReset();
	}
}
