﻿using System.Collections;
using UnityEngine;
using Common.Constants;

namespace Gameplay.Interactives
{
	public class Flower : Resetable
	{
		[SerializeField]
		private SpriteRenderer view;

		[SerializeField]
		private Transform head;

		[SerializeField]
		[Range(0.25f, 25)]
		private float height = 1;
		private float initialHeight;
		public float Height
		{
			get => height;
			set
			{
				height = value;
				if (view)
					view.size = new Vector2(view.size.x, height);
				if (head)
					head.localPosition = new Vector3(0, height + headOffset);
			}
		}

		[SerializeField]
		[Range(-1, 1)]
		private float headOffset = 0;

		public void AnimateHeight(float targetHeight)
		{
			if (animateHeightRoutine != null)
				StopCoroutine(animateHeightRoutine);

			animateHeightRoutine = StartCoroutine(AnimateHeightRoutine(targetHeight));
		}

		Coroutine animateHeightRoutine = null;
		private IEnumerator AnimateHeightRoutine(float targetHeight)
		{
			while (Mathf.Abs(height - targetHeight) > 0.05f)
			{
				Height += (targetHeight - Height) * Time.fixedDeltaTime * 12;
				yield return WaitFor.FixedUpdate;
			}

			Height = targetHeight;

			animateHeightRoutine = null;
		}

		private void OnValidate() => Height = Height;

		private void Awake() => initialHeight = Height = Height;

		protected override void OnReset() => Height = initialHeight;
	}
}
