﻿using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
	public class PlayerTrigger : MonoBehaviour
	{
		[SerializeField]
		private UnityEvent onEnter = new UnityEvent();
		public UnityEvent OnEnter => onEnter;

		[SerializeField]
		private UnityEvent onExit = new UnityEvent();
		public UnityEvent OnExit => onExit;

		private void OnTriggerEnter2D(Collider2D collider)
		{
			if (collider.CompareTag("Player"))
				OnEnter.Invoke();
		}

		private void OnTriggerExit2D(Collider2D collider)
		{
			if (collider.CompareTag("Player"))
				OnExit.Invoke();
		}
	}
}