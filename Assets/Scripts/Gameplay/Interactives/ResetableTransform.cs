﻿using UnityEngine;

namespace Gameplay.Interactives
{
	public class ResetableTransform : Resetable
	{
		private Vector3 initialPosition;
		private Quaternion initialRotation;
		private Vector3 initialScale;

		private void Awake()
		{
			initialPosition = transform.position;
			initialRotation = transform.rotation;
			initialScale = transform.localScale;
		}

		protected override void OnReset()
		{
			transform.position = initialPosition;
			transform.rotation = initialRotation;
			transform.localScale = initialScale;
		}
	}
}