﻿using Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Gameplay
{
	public class CheckPoint : MonoBehaviour
	{
		[SerializeField]
		private Transform restorePlace;
		public Transform RestorePlace => restorePlace;

		[SerializeField]
		private UnityEvent onInteract = new UnityEvent();

		private void Start() => onInteract.AddListener(CheckIn);

		private void OnTriggerEnter2D(Collider2D collider)
		{
			if (enabled && collider.CompareTag("Player"))
				onInteract.Invoke();
		}

		private void CheckIn()
		{
			var level = SceneManager.GetActiveScene().buildIndex;
			var index = GameplayController.Instance.GetCheckPointIndex(this);
			var progress = Progress.Instance;

			var dataCeckPoint = new Progress.CheckPoint(level, index);
			progress.AddCheckPoint(dataCeckPoint);
			progress.CurrentCheckPoint = dataCeckPoint;
		}
	}
}
